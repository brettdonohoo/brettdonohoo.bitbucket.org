var KEYCODE_SPACE = 32; 	//usefull keycode
var KEYCODE_UP = 38; 	//usefull keycode
var KEYCODE_LEFT = 37; 	//usefull keycode
var KEYCODE_RIGHT = 39; 	//usefull keycode
var KEYCODE_W = 87; 		//usefull keycode
var KEYCODE_A = 65; 		//usefull keycode
var KEYCODE_D = 68; 		//usefull keycode


var canvas;
var stage;
var screen_width;
var screen_height;
var bmpAnimation;
var bmpAnimationIdle;
var Hero;
var contentManager;

var numberOfImagesLoaded = 0;

var imgPlayer = new Image();

function init() {
	canvas = document.getElementById("testCanvas");

  // create a new stage and point it at our canvas:
  stage = new createjs.Stage(canvas);
  // grab canvas width and height for later calculations:

  screen_width = canvas.width;
  screen_height = canvas.height;
	
  contentManager = new ContentManager();
	contentManager.SetDownloadCompleted(startGame);
	contentManager.StartDownload();
}

function reset() {
	stage.removeAllChildren();
	stage.update();
	createjs.Ticker.init();
}

function suspend(btn) {
	if($(btn).html() == "Pause")
	{
		createjs.Ticker.setPaused(true);
		$(btn).html("Resume");
	}
	else
	{
		createjs.Ticker.setPaused(false);
		$(btn).html("Pause");
	}	
}


function startGame() {
	// Our hero can be moved with the arrow keys (left, right)
	document.onkeydown = handleKeyDown;
	document.onkeyup = handleKeyUp;

	Hero = new Player(contentManager.imgPlayer, screen_width);
	Hero.x = 16;
	Hero.y = 150;

	stage.addChild(Hero);
	
	// we want to do some work before we update the canvas,
	// otherwise we could use Ticker.addListener(stage);
	createjs.Ticker.addListener(window);
	createjs.Ticker.useRAF = true;
	createjs.Ticker.setFPS(60);
}

function tick() {
	Hero.tick();

	// update the stage:
	stage.update();
}

function handleKeyDown(e) {
    //cross browser issues exist
    if (!e) { var e = window.event; }
    switch (e.keyCode) {
        case KEYCODE_W: 
        case KEYCODE_UP: 
            if (Hero.alive && Hero.isInIdleMode) {
                if (Hero.direction == 1)
								{
									Hero.gotoAndPlay("celebrate_h");
								}
								else
								{
									Hero.gotoAndPlay("celebrate");
								}
						}
												
						break;
        case KEYCODE_SPACE: 
            if (Hero.alive) {
                if (Hero.direction == 1)
								{
									Hero.gotoAndPlay("jump_h");
								}
								else
								{
									Hero.gotoAndPlay("jump");
								}
            }

            break;
        case KEYCODE_A: ;
        case KEYCODE_LEFT:
            // We're launching the walk_left animation
            if (Hero.alive && Hero.isInIdleMode) {
                Hero.gotoAndPlay("walk");
                Hero.direction = -1;
                Hero.isInIdleMode = false;
            }

            break;

        case KEYCODE_D: ;
        case KEYCODE_RIGHT:
            // We're launching the walk_right animation
            if (Hero.alive && Hero.isInIdleMode) {
               Hero.gotoAndPlay("walk_h");
                Hero.direction = 1;
                Hero.isInIdleMode = false;
            }

            break;
    }
}

function handleKeyUp(e) {
    //cross browser issues exist
    if (!e) { var e = window.event; }
    switch (e.keyCode) {
        case KEYCODE_W: ;
        case KEYCODE_UP: ;
        case KEYCODE_SPACE:
            if (Hero.alive) {
							if(Hero.isInIdleMode)
							{
                Hero.gotoAndPlay("idle");
							}
							else
							{
								if(Hero.direction == 1)
								{
									Hero.gotoAndPlay("walk_h");
								}
								else
								{
									Hero.gotoAndPlay("walk");
								}
							}
            }
						break;
        case KEYCODE_A: ;
        case KEYCODE_LEFT: ;  
        case KEYCODE_D: ;
        case KEYCODE_RIGHT:
            if (Hero.alive) {
               Hero.isInIdleMode = true;
                Hero.gotoAndPlay("idle");
            }
            break;
    }
}


